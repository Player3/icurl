# ICurl
For when you're on a box without curl or wget, but really need to download something.

## Code

  ```
  function __curl() {
    read proto server path <<<$(echo ${1//// })
    DOC=/${path// //}
    HOST=${server//:*}
    PORT=${server//*:}
    [[ x"${HOST}" == x"${PORT}" ]] && PORT=80

    exec 3<>/dev/tcp/${HOST}/$PORT
    echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
    (while read line; do
    [[ "$line" == $'\r' ]] && break
    done && cat) <&3
    exec 3>&-
  }
  ```


## Usage
  ```
  user@server:/tmp/$ function __curl() {
  >   read proto server path <<<$(echo ${1//// })
  >   DOC=/${path// //}
  >   HOST=${server//:*}
  >   PORT=${server//*:}
  >   [[ x"${HOST}" == x"${PORT}" ]] && PORT=80
  > 
  >   exec 3<>/dev/tcp/${HOST}/$PORT
  >   echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  >   (while read line; do
  >    [[ "$line" == $'\r' ]] && break
  >   done && cat) <&3
  >   exec 3>&-
  > }
  user@server:/tmp/$ __curl http://127.0.0.1:8000/juice.txt > juice.txt 
  ```

## Credit
https://unix.stackexchange.com/questions/83926/how-to-download-a-file-using-just-bash-and-nothing-else-no-curl-wget-perl-et
